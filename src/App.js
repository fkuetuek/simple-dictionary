import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from 'react';
import './App.css';
import WordCard from './components/WordCard';

const API_URL = 'https://api.dictionaryapi.dev/api/v2/entries/en/';

function App() {
  const [searchResult, setSearchResult] = useState('');
  const [userSearchWord, setUserSearchWord] = useState('');


  const searchWord = async (word) => {
    const response = await fetch(`${API_URL}${word}`);
    const data = await response.json();
    setSearchResult(data);
  }



  return (
    <div className="App">

      {/* NavBar */}
      <nav className="navbar navbar-expand bg-primary">
        <div className="container-fluid justify-content-center">
          <a className="navbar-brand text-light" href="#">
            Simple Dictionary
          </a>
        </div>
      </nav>

      <section>
        <div className="container mt-5">
          <div className="input-group mb-3 max-input-width">
            <input 
              type="text" 
              placeholder='search word' 
              value={userSearchWord}
              onChange={(e) => setUserSearchWord(e.target.value)}
              className="form-control" 
            />
            <button
              type='submit' 
              onClick={() => searchWord(userSearchWord)}
              className="btn btn-primary"> 
              Search 
              </button>
          </div>

        </div>
      </section>
      
      <section>
        <div className="container">
          <div className="row justify-content-center">
            {
              searchResult?.length > 0 ? (
                  <WordCard 
                  word={searchResult[0].word}
                  phonetic={searchResult[0].phonetic}
                  type={searchResult[0].meanings[0].partOfSpeech}
                  meanings={searchResult[0].meanings}
                  definition={searchResult[0].meanings[0].definitions[0].definition}
                  pronunciation={searchResult[0].phonetics[0].audio}
                  alldef={searchResult[0].meanings[0].definitions}
                />
              ) : (
                <></>
              )
            } 
          </div>
        </div>
      </section>      
    </div>
  );
}

export default App;
