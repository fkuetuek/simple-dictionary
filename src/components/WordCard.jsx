import './WordCard.css'
import { v4 as uuidv4 } from 'uuid';
import {FaVolumeOff} from 'react-icons/fa'


const WordCard = ({ word, phonetic, type, meanings, definition, pronunciation, alldef }) => {
    const definitions = alldef;
    let id = 1;
    
    function incrementID() {
        id += 1;
        return id;
    }
    
    const definitionList = definitions.map((def) =>
        <>
            <p key={incrementID()}>{id}- {def.definition}</p>
            {
                def.example?.length > 0 ? (
                    <p key={uuidv4()}>Ex: {def.example}</p>  
                ):(
                    console.log("no example")
                )
            }
        </> 
   
    );
    function playAudio() {
        var audio = document.getElementById('audio');
        audio.play();
    }

    return (
        <div className="col col-lg-8">
            <div className="card max-card-width">
                <div className="card-header"><span className="fs-1">{word}</span>
                <span className="fs-6"> {phonetic}
                    {
                        pronunciation !== '' ? (
                            <>
                                <audio id='audio' src={pronunciation} controls>
                                    Your browser does not support the audio element.
                                </audio>
                                <span onClick={() => playAudio()} > <FaVolumeOff /></span>
                            </>
                    )  : (<p>no audio</p>)
                    }
                </span>
                </div>
                
                <div className="card-body text-start">{definitionList}</div>
            </div>
        </div>
    );
}


export default WordCard;



